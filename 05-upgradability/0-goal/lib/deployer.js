const {create} = require('chain-dsl')

const base = async (web3, contractRegistry, DEPLOYER) => {
    const deploy = (...args) => create(web3, DEPLOYER, ...args)

    const {
        Token
    } = contractRegistry

    const token = await deploy(Token)

    return {token}
}

module.exports = {
    base
}
