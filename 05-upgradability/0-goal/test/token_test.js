const {
    expect,
    expectAsyncThrow,
    expectNoAsyncThrow,
    expectThrow,
    solc,
    ganacheWeb3,
} = require('chain-dsl/test/helpers')

const deploy = require('../lib/deployer')
const {send} = require('chain-dsl')

describe('Token', function () {
    let web3, snaps, accounts, DEPLOYER, OPERATOR, CUSTOMER, token
    const wad = 10

    before('deployment', async () => {
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            CUSTOMER,
            NOT_CUSTOMER
        ] = accounts = await web3.eth.getAccounts()

        ;({token} = await deploy.base(web3, solc(__dirname, '../solc-input.json'), DEPLOYER, OPERATOR))
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))

    it("allows transfer to whitelisted address", async function () {
        await send(token, DEPLOYER, 'whitelist', CUSTOMER)

        await expectNoAsyncThrow(async () =>
            send(token, DEPLOYER, 'transfer', CUSTOMER, wad))
    })

    it("rejects transfer to non-whitelisted address", async function () {
        await expectThrow(async () =>
            send(token, DEPLOYER, 'transfer', NOT_CUSTOMER, wad))
    })

})
