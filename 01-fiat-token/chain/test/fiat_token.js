const FiatToken = artifacts.require("./FiatToken.sol");

contract('FiatToken', (accounts) => {
    it("can mint and burn", async () => {
        const t = await FiatToken.deployed();
        await t.mint(accounts[0], 100);
        await t.burn(10);
        assert.equal((await t.balanceOf.call(accounts[0])).valueOf(), 90);
    });
});
