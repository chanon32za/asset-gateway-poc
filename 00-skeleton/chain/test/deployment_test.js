const {expect, solc, ganacheWeb3, logAccounts} = require('chain-dsl/test/helpers')
const {call} = require('chain-dsl')
const deployer = require('../lib/deployer')

describe('Deployment', () => {
    let web3, snaps, accounts, DEPLOYER, OPERATOR, CUSTOMER, metaCoin, lab

    before('deployment', async () => {
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            OPERATOR,
            CUSTOMER
        ] = accounts = await web3.eth.getAccounts()

        ;({metaCoin, lab} = await deployer.base(web3, solc(__dirname, '../solc-input.json'), DEPLOYER))
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))
    after(() => logAccounts(accounts))

    it('Lab is deployed', async () => {
        const meaningOfLife = await lab.methods.meaningOfLife().call()
        expect(meaningOfLife).eq(42)
    })

    it.skip('MetaCoin is deployed', async () => {
        const sym = web3.utils.hexToUtf8((await call(metaCoin, 'getBalance')))
        expect(sym).equal('TOKUSD')
    })
})
