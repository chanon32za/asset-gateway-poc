const {
    bytes32,
    address,
    wad,
    sig,
    send,
    call,
    create
} = require('chain-dsl')

const base = async (web3, contractRegistry, DEPLOYER, OPERATOR) => {
    const deploy = (...args) => create(web3, DEPLOYER, ...args)

    const {
        FiatToken, Gateway,
    } = contractRegistry

    const token = await deploy(DSToken, web3.utils.utf8ToHex('TOKUSD'))

    return {
        token
    }
}

module.exports = {
    base
}
