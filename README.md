# Proof of concept system for OAX asset gateways

## Basic functionality

![](doc/img/core.png)

## Structure

The top-level, numbered directories correspond to milestones or aspects of the system.
These directories have a corresponding README, which describe how to use them, eg: [`02-money-in/README.md`]()

The `chain-dsl/` directory is a nodejs package providing blockchain
access convenience layer. 

## Setup
 
```bash
npm install -g pnpm
pnpm recursive install
pnpm recursive link
```

## Design decisions

We rely on the [Dappsys](https://dapp.tools/dappsys/) libraries in
our smart contracts, because they have a more comprehensive
and configurable authentication story as opposed to
[OpenZeppelin](https://github.com/OpenZeppelin/zeppelin-solidity).

We are following the development of [zeppelinOS](https://zeppelinos.org/)
in the hope of adopting their contract upgrade approach on the longer
term.On the short term, we rely on a plugin architecture which has
stronger trust guarantees, at a cost of flexibility.

We tried [Truffle v4.0.1](https://github.com/trufflesuite/truffle/releases/tag/v4.0.1)
in hope of getting a convenient development workflow out of the box.
We ended up fighting a lot with the tooling, so instead we built a
thin integration layer around `ganache-core`, which is the same
EVM implementation which Truffle relies on. 

We also try to stay on the latest Solidity version to be prepared
to benefit from the features coming up in Metropolis.

To ascertain correct operation on the mainnet, the developed 
smart contract code should be tested against:

1. some development chain, for a tight feedback loop, eg:
    1. `ganache-core`
    1. `geth --dev`
    1. `dapp testnet` ([Hevm](https://dapp.tools/hevm/))
1. Rinkeby, to be even closer to the properties of the mainnet
