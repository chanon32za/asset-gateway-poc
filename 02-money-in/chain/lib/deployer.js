const {address, sig, call, send, create} = require('chain-dsl')

const base = async (web3, contractRegistry, DEPLOYER, OPERATOR) => {
    const deploy = (...args) => create(web3, DEPLOYER, ...args)

    const {
        Gate,
        AccessControl
    } = contractRegistry

    const accessControl = await deploy(AccessControl)
    const gate = await deploy(Gate, address(accessControl))

    const OPERATOR_ROLE = await call(accessControl, 'OPERATOR')

    const allow = method => {
        return send(accessControl, DEPLOYER, 'setRoleCapability',
            OPERATOR_ROLE, address(gate), sig(method), true)
    }

    const methodsAllowedForOperator = [
        'balanceOf(address)',
        'mint(uint256)',
        'mint(address,uint256)',
        'mintFor(address,uint256)',
        'burn(address,uint256)',
        'burn(uint256)',
        'burnFrom(address,uint256)',
    ]

    await Promise.all([
        send(accessControl, DEPLOYER, 'setUserRole',
            OPERATOR, OPERATOR_ROLE, true),
        ...methodsAllowedForOperator.map(allow)
    ])

    return {gate, accessControl}
}

module.exports = {
    base
}
