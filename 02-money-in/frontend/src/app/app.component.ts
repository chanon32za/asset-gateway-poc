import { Component } from '@angular/core';

import {OAuthService} from 'angular-oauth2-oidc';
import {JwksValidationHandler} from 'angular-oauth2-oidc';
import {authConfig} from './config/auth.config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';


  constructor(private oauthService: OAuthService) {

    this.configureWithNewConfigApi()

  }


  private configureWithNewConfigApi() {
    this.oauthService.configure(authConfig);
    this.oauthService.tryLogin({});
  }

}
