import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import { Location } from '@angular/common';
import {Web3Service} from "../../util/web3.service";
import {accountConfig} from "../../config/account.config";
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-gateway-withdraw-submit',
  templateUrl: './gateway-withdraw-submit.component.html',
  styleUrls: ['./gateway-withdraw-submit.component.scss']
})
export class GatewayWithdrawSubmitComponent implements OnInit {
  isshow = false;


  model = {
    amount: 0,
  };

  layouts = [
    {text: 'One', cols: 4, rows: 4, color: 'lightblue'},
    {text: 'Two', cols: 12, rows: 12, color: 'lightgreen'},
    {text: 'Three', cols: 4, rows: 8, color: 'lightpink'},
  ];

  config = accountConfig;
  constructor(
    private authService: AuthService,
    private location: Location,
    private _web3Service: Web3Service,
    private _router: Router,
    public snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
  }


  async submit() {

    if (this.model.amount <= 0) {
      this.snackBar.open("Amount is negative", "Okay", {
        duration: 2000,
      });
      return;
    }
    await this._web3Service.withdraw(this.model.amount)
    this._router.navigate(["/en-US/gateway/dashboard/withdraw/transfer"])

  }

  goBack(): void {
    this.location.back();
  }

  showdiv(): void {
    this.isshow = true;
  }
  hidediv(): void {
    this.isshow = false;
  }


}
