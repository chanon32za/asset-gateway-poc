To view api spec swagger.yaml smoothly,
fire up [swagger-editor](https://github.com/swagger-api/swagger-editor) locally 
and paste swagger.yaml in the editor.

#NOTE
- Only swagger 2.0 is supported smoothly by this editor.
- http://jsonapi.org/format/ is referenced when designing API.
  Please do let me know if any convention specified [there](http://jsonapi.org/format/) is breached.
- the swagger editor generates both server and client side code for the api implementations, simply mind blowing.