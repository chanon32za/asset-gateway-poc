# 02. _Money in_ phase

After exploring the potential interactions with banks, we stepped back
and abstracted away those steps.

## Further documentation

* Chain [README](chain/README.md)

## [money-in.puml](money-in.puml) diagram

This flow explores what information would flow through customer and asset gateway bank.

## [money-in-compact.puml](money-in-compact.puml) diagram

Similar to [money-in.puml]() with less fancy, hence more compacy parameter notation.
It might be less readable but easier to modify.
