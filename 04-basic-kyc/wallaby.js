module.exports = function () {
    return {
        files: [
            'contracts/**/*.sol',
            'lib/**/*.js',
            'test/helpers.js',
            'solc-input.json'
        ],

        tests: [
            'test/*_test.js'
        ],
        env: {
            type: 'node'
        },
        testFramework: 'mocha'
    }
}
